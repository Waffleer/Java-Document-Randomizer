package application;

import java.util.Scanner;
 
class GetInputFromUser {
    public static void main(String args[])
    {
        // Using Scanner for Getting Input from User
        Scanner scan = new Scanner(System.in);
 
        String s = scan.nextLine();
        System.out.println("You entered string " + s);
 
        int a = scan.nextInt();
        System.out.println("You entered integer " + a);
 
        float b = scan.nextFloat();
        System.out.println("You entered float " + b);
       
          // closing scanner
          scan.close();
    }
}