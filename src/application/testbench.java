package application;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*; //import java swing to use the Jframe etc


public class testbench {

    public static void main(String[] args)
    {
        JFrame app = new JFrame("Chat Frame");
        app.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        app.setSize(500, 500);

        frameFill(app);
        //frameClear(app)


    }
    public static void frameFill(JFrame frame){

        //Top Bar Creation - not implimented yet
        JMenuBar topbar = new JMenuBar();
        JMenu fileM = new JMenu("File");
        JMenu helpM = new JMenu("Help");
        JMenu configM = new JMenu("Config Session");

        topbar.add(fileM);
        topbar.add(configM);
        topbar.add(helpM);
        
        //File Menu
        JMenuItem openB = new JMenuItem("Open");
        JMenuItem saveB = new JMenuItem("Save");
        JMenuItem save_asB = new JMenuItem("Save as");

        fileM.add(openB);
        fileM.add(saveB);
        fileM.add(save_asB);

        //Config Menu
        JMenuItem sessionNameB = new JMenuItem("Session Name");
        JMenuItem gradingB = new JMenuItem("Grading");
        JMenuItem folderB = new JMenuItem("File Locations");
        JMenuItem newSessionB = new JMenuItem("New Session");

        configM.add(newSessionB);
        configM.add(sessionNameB);
        configM.add(gradingB);
        configM.add(folderB);



        //Creating the panel at bottom and adding components
        JPanel panel = new JPanel(); // the panel is not visible in output
        JButton send = new JButton("Send");
        JButton reset = new JButton("Reset");
        panel.add(send);
        panel.add(reset);

        // Text Area at the Center
        JTextArea ta = new JTextArea();


        
        //Adding Components to the frame. - implementing
        
        frame.getContentPane().add(BorderLayout.SOUTH, panel);
        frame.getContentPane().add(BorderLayout.CENTER, ta);
        frame.getContentPane().add(BorderLayout.NORTH, topbar);
        frame.setVisible(true);




        // File menu
        openB.addActionListener(new ActionListener(){  
            public void actionPerformed(ActionEvent e){  
                    JBopen();
                    }});  

        saveB.addActionListener(new ActionListener(){  
            public void actionPerformed(ActionEvent e){  
                    JBsave();
                    }});  

        save_asB.addActionListener(new ActionListener(){  
            public void actionPerformed(ActionEvent e){  
                    JBsave_as();
                    }}); 




        //Config Menu
        sessionNameB.addActionListener(new ActionListener(){  
            public void actionPerformed(ActionEvent e){  
                    JBsessionName();
                    }});  

        gradingB.addActionListener(new ActionListener(){  
            public void actionPerformed(ActionEvent e){  
                    JBgrading();
                    }});  

        folderB.addActionListener(new ActionListener(){  
            public void actionPerformed(ActionEvent e){  
                    JBfolder();
                    }}); 

        newSessionB.addActionListener(new ActionListener(){  
            public void actionPerformed(ActionEvent e){  
                    JBnewSession();
                    }}); 






        // Other Buttons
        send.addActionListener(new ActionListener(){  
            public void actionPerformed(ActionEvent e){  
                    JBsend();

                    }});  


        reset.addActionListener(new ActionListener(){  
            public void actionPerformed(ActionEvent e){  
                    JBreset();

                    }});  



    }




    //Bottom Panel
    public static void JBsend(){
        System.out.println("send pressed");
    }

    public static void JBreset(){
        System.out.println("reset pressed");
    }


    //File menu
    public static void JBopen(){
        System.out.println("open pressed");
        //open up json file
    }

    public static void JBsave(){
        System.out.println("save pressed");
        //write json files
    }

    public static void JBsave_as(){
        System.out.println("save as pressed");
        // write json files
    }


    // Config Session Menu
    public static void JBnewSession(){
        System.out.println("New Session pressed");
        
        sessionCreate.sessionC();




        //activeSession = true;

        // creates new json file 
        // opens session creater
    }

    public static void JBsessionName(){
        System.out.println("Session Name pressed");
        // change current session name
        // changes json files
    }

    public static void JBgrading(){
        System.out.println("Grading pressed");
        // changes json file
    }

    public static void JBfolder(){
        System.out.println("Folder pressed");
        //changes json folder path
    }



    // Other Funtions
    public static void frameClear(JFrame frame){
        frame.getContentPane().removeAll();
        frame.getContentPane().repaint();

    }

}
