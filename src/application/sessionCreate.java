package application;

import java.io.FileWriter;
import java.io.IOException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
 
public class sessionCreate
{
    @SuppressWarnings("unchecked")
    public static void sessionC()
    {
        System.out.println("Session Create has run");
        String sessionName = "testing";
        String sessionPath = "src/application/sessions/path/";

        //First Employee
        JSONObject sessionInfo = new JSONObject();
        sessionInfo.put("session name", sessionName);
        sessionInfo.put("grading", "0/0");
        sessionInfo.put("path", sessionPath);

        JSONObject session = new JSONObject(); 
        session.put("session", sessionInfo);
         
        //Add employees to list
        JSONArray sessionList = new JSONArray();
        sessionList.add(session);
        
        //Write JSON file
        try (FileWriter file = new FileWriter("src/application/sessions/"+sessionName+".json")) {
            //We can write any JSONArray or JSONObject instance to the file
            file.write(sessionList.toJSONString()); 
            file.flush();
 
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
