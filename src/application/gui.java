package application;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*; //import java swing to use the Jframe etc


public class gui {



    public static void main(String[] args)
    {
        JFrame app = new JFrame("Chat Frame");
        app.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        app.setSize(400, 400);



        frameFill(app);
        //frameClear(app)


    }
    public static void frameFill(JFrame frame){

        //Top Bar Creation - not implimented yet
        JMenuBar topbar = new JMenuBar();
        JMenu fileM = new JMenu("File");
        JMenu helpM = new JMenu("Help");
        topbar.add(fileM);
        topbar.add(helpM);
        
        JMenuItem openB = new JMenuItem("Open");
        JMenuItem save_asB = new JMenuItem("Save as");
        fileM.add(openB);
        helpM.add(save_asB);




        //Creating the panel at bottom and adding components
        JPanel panel = new JPanel(); // the panel is not visible in output
        JLabel label = new JLabel("Enter Text");
        JTextField tf = new JTextField(10); // accepts upto 10 characters
        JButton send = new JButton("Send");
        JButton reset = new JButton("Reset");
        panel.add(label); // Components Added using Flow Layout
        panel.add(tf);
        panel.add(send);
        panel.add(reset);

        // Text Area at the Center
        JTextArea ta = new JTextArea();


        
        //Adding Components to the frame. - implementing
        frame.getContentPane().add(BorderLayout.SOUTH, panel);
        frame.getContentPane().add(BorderLayout.NORTH, topbar);
        frame.getContentPane().add(BorderLayout.CENTER, ta);
        frame.setVisible(true);


        send.addActionListener(new ActionListener(){  
            public void actionPerformed(ActionEvent e){  
                    JBsend();

                    }});  


        reset.addActionListener(new ActionListener(){  
            public void actionPerformed(ActionEvent e){  
                    JBreset();

                    }});  
            

    }
    public static void JBsend(){
        System.out.println("send pressed");
    }


    public static void JBreset(){
        System.out.println("reset pressed");
    }

    public static void frameClear(JFrame frame){
        frame.getContentPane().removeAll();
        frame.getContentPane().repaint();

    }

}